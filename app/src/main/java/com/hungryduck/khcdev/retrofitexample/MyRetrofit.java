package com.hungryduck.khcdev.retrofitexample;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by chakh on 2017-10-04.
 *  Retrofit 객체는 rest 요청/응답을 하기 위한 서버의 baseurl, 응답객체 converter 등의 정보를 담고 있다.
 *  요청을 하기 위한 서비스 객체를 만들기 위해 사용된다.
 *
 *  retrofit 객체의 안정적인 사용을 위해 싱글턴 패턴으로 구현했다.
 */

public class MyRetrofit {
    private static MyRetrofit instance = new MyRetrofit();
    private Retrofit retrofit;

    private MyRetrofit(){
        buildRetrofit();
    }
    private void buildRetrofit(){
        retrofit= new Retrofit.Builder()
                //서버의 base url 주소 등록
                .baseUrl("http://52.78.234.100:3000/")
                //json객체를 converting 해주는 라이브러리를 import 해야한다. build.gradle을 참고한다.
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
    public static MyRetrofit getInstance(){
        return instance;
    }
    public Retrofit getRetrofit(){
        return retrofit;
    }
}
