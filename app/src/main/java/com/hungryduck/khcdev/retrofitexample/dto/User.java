package com.hungryduck.khcdev.retrofitexample.dto;

/**
 * Created by chakh on 2017-10-05.
 */

// case  : 응답 객체가 단일 객체인경우
    /**
     * {
     *     name:"",
     *     age:"",
     *     email
     * }
     * */
public class User {
    public String name;
    public int age;
    public String email;
}
