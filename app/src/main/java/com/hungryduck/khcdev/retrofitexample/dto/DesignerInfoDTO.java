package com.hungryduck.khcdev.retrofitexample.dto;

/**
 * Created by chakh on 2017-10-05.
 */

// case : json 객체 내에 객체
    /**
     * ex)
     * { < json객체
     *     profile:{ < 그안에 객체 하나 더
     *         nickname:"",
     *         introduce:"",
     *         portimg_number: 0
     *     }
     * }
     * */
    //-> 서버단에서 단일 프로퍼티와 객체가 복합적인경우가 아니라면 단일 객체안에 단일 객체의 형태는 지양하도록 한다.
public class DesignerInfoDTO {
        // 객체 자체가 DTO 모델로 들어오기 떄문에 json내에 객체가 하나 이상 존재하는 경우는 클래스를 만들어
    public Profile profile;
    public class Profile {
        public String nickname;
        public String introduce;
        public int portimg_number;
    }
}

