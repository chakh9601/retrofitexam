package com.hungryduck.khcdev.retrofitexample.Interface;

import com.hungryduck.khcdev.retrofitexample.dto.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by chakh on 2017-10-05.
 */

public interface PostDataBody {
    //Post 요청을 보내는 예제 기본 골조는 GET메세지랑 비슷하나 Body 데이터를 어떻게 넘길 것인지 urlencoded는 값을 어떻게 주는지에 대한 방법을 알아본다
    @POST("user/new")
    // 파라미터로 전해진 User타입의 객체를 body로써 사용한다.
    Call<User> createUser(@Body User user);

}
