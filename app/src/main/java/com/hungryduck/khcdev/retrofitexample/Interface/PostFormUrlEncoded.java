package com.hungryduck.khcdev.retrofitexample.Interface;

import com.hungryduck.khcdev.retrofitexample.dto.User;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by chakh on 2017-10-05.
 */

public interface PostFormUrlEncoded {
    // Body를 전달하는것과 다르게 urlencoded form으로 데이터를 주고 싶은 경우엔 다음과 같이 작성한다.
    @FormUrlEncoded
    @POST("user/new")
    //key-value 형태 이므로 Field 어노테이션을 사용한다.
    Call<User> postData(
            @Field("name") String name,
            @Field("age") int age,
            @Field("email")String email
    );
}
