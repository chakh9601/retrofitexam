package com.hungryduck.khcdev.retrofitexample.Interface;

import com.hungryduck.khcdev.retrofitexample.dto.User;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by chakh on 2017-10-05.
 */

public interface PostMedia {
    @Multipart
    @POST("article/new")
    Call<User> userArticle(@Part("image") RequestBody image, @Part("description") RequestBody description);


}
