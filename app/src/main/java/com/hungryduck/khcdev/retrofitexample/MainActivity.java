package com.hungryduck.khcdev.retrofitexample;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.hungryduck.khcdev.retrofitexample.Interface.GetDesignerInfo;
import com.hungryduck.khcdev.retrofitexample.dto.DesignerInfoDTO;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;

/**
 * Retroft 사용법에 관한 문서/ 예제
 * 사용법에 대한 것들 최대한 상세하게 작성 하였으니 보고 사용하시면 될것 같습니다.
 * */

/**
 * Retrofit 구성 방법
 * 구성방법은 크게 다음과 같다.
 * 1. Retrofit 싱글턴으로써 구현 -> MyRetrofit 클래스 참고
 * 2. 응답 모델(dto)를 작성한다 + 응답으로 온 객체가 activity간 데이터 전송에 사용되는 경우 serializaed 되어야 한다. 응답모델은 dto를 참고한다.
 * 3. 어떤 요청에 대한 인터페이스를 만들어야 한다. interface 패키지의 인터페이스들 참고, 인터페이스 작성방법은 GetDesignerInfo 인터페이스 참고한다.
 * 4. 서비스를 만들어 요청한다.
 * */
public class MainActivity extends AppCompatActivity {
    Button getDesignerInfo_btn;
    TextView designerinfo_tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getDesignerInfo_btn =(Button)findViewById(R.id.designerInfo);
        designerinfo_tv = (TextView) findViewById(R.id.designerinfo_tv);
        getDesignerInfo_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //요청을 위한 서비스 만들기
                GetDesignerInfo designerInfoService = MyRetrofit.getInstance().getRetrofit().create(GetDesignerInfo.class);
                final Call<DesignerInfoDTO> call = designerInfoService.designerInfo("23");
                //네트워크 작업은 asynctask에서 진행한다.
                new NetworkCall().execute(call);
            }
        });
    }


    private class NetworkCall extends AsyncTask<Call,Void, DesignerInfoDTO>{
        @Override
        protected DesignerInfoDTO doInBackground(Call ... params){
            try{
                Call<DesignerInfoDTO> call = params[0];
                Response<DesignerInfoDTO> response = call.execute();
                return response.body();
            }catch(IOException e){
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(DesignerInfoDTO result) {
            designerinfo_tv.setText("이름 : "+result.profile.nickname + ",\n 소개 : "+ result.profile.introduce + ",\n 포트폴리오 개수 : "+ result.profile.portimg_number);
        }
    }
}



